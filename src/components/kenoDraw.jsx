import React, { Component } from "react";

class KenoDraw extends Component {
  state = {
    ticket: {
      tktElements: [],
      tktClass: new Array(9)
    },
    draw: [],
    disableDraw: true,
    disableTkt: false,
    disableReset: true
  };

  /*
function: generateNos
scope: generate random number between 1 to 80
input: limit-> indicates number of random numbers needed(10 or 20) 
output: array of numbers
  */

  generateNos = limit => {
    let nos = [];
    let randNo;
    while (nos.length < limit) {
      randNo = Math.floor(Math.random() * 80) + 1;
      if (nos.indexOf(randNo) === -1) nos.push(randNo);
    }

    nos.sort((a, b) => a - b);

    if (limit === 10) {
      let tktObj = {
        tktElements: nos,
        tktClass: new Array(9)
      };
      this.setState({
        ticket: tktObj,
        disableDraw: false,
        disableReset: false
      });
    } else {
      let tktObj = {
        tktElements: this.state.ticket.tktElements,
        tktClass: new Array(9)
      };

      let matchedItems = nos.filter(element =>
        this.state.ticket.tktElements.includes(element)
      );

      for (let i = 0; i < matchedItems.length; i++) {
        let indx = this.state.ticket.tktElements.indexOf(matchedItems[i]);
        if (indx !== -1) tktObj.tktClass[indx] = "font-highlight"; // if there is a match, update class to highlight
      }

      this.setState({
        draw: nos,
        ticket: tktObj,
        disableDraw: true,
        disableTkt: true
      });
    }
  };

  addPadding = item => {
    return (item < 10 ? "0" : "") + item; //add 0 as padding for the numbers
  };

  /*
function: getTicketTemplate
scope: generate the template for create ticket functionality
output: array consists of label and a div which consists of ticket info
  */

  getTicketTemplate() {
    const { tktElements, tktClass } = this.state.ticket;
    let tktTemplate = [];
    let i = 0;
    tktTemplate[0] = (
      <div className="col-3">
        <label>Your ticket is: </label>
      </div>
    );

    tktTemplate[1] = (
      <div className="col-3 tkt-height">
        {tktElements.map(tkt => {
          i++;
          return (
            <div
              key={tkt}
              className={"box-item " + (tktClass[i - 1] ? tktClass[i - 1] : "")}
            >
              {this.addPadding(tkt)}
            </div>
          );
        })}
      </div>
    );

    return tktTemplate;
  }

  /*
function: getdrawTemplate
scope: generate the template for start draw functionality
output: array consists of label and a div which consists of draw info
  */

  getdrawTemplate() {
    let drawTemplate = [];
    drawTemplate[0] = (
      <div className="col-3">
        <label>The Lucky draw is: </label>
      </div>
    );
    drawTemplate[1] = (
      <div className="col-3">
        {this.state.draw.map(draw => (
          <div key={draw} className="box-item">
            {this.addPadding(draw)}
          </div>
        ))}
      </div>
    );

    return drawTemplate;
  }

  /*
function: handleReset
scope: reset the screen to its original position by reverting the state to its intial value
output: NA
  */

  handleReset = () => {
    let ticket = {
      tktElements: [],
      tktClass: new Array(9)
    };
    this.setState({
      ticket: ticket,
      draw: [],
      disableDraw: true,
      disableTkt: false,
      disableReset: true
    });
  };

  render() {
    let tktTemplate = [];
    let drawTemplate = [];
    const { ticket, draw, disableTkt, disableDraw, disableReset } = this.state;

    if (ticket.tktElements.length > 0) {
      tktTemplate = this.getTicketTemplate();
    }

    if (draw.length > 0) {
      drawTemplate = this.getdrawTemplate();
    }

    return (
      <div>
        <h1>Keno Jackpot</h1>
        <button
          disabled={disableTkt}
          onClick={() => this.generateNos(10)}
          className="btn btn-primary btn-sm"
        >
          Get my ticket
        </button>
        <button
          disabled={disableDraw}
          onClick={() => this.generateNos(20)}
          className="btn btn-primary btn-sm m-3"
        >
          Start Draw
        </button>
        <button
          disabled={disableReset}
          onClick={this.handleReset}
          className="btn btn-success btn-sm"
        >
          Reset
        </button>
        <div className="row">
          {tktTemplate[0] ? tktTemplate[0] : null}{" "}
          {drawTemplate[0] ? drawTemplate[0] : null}
        </div>
        <div className="row">
          {tktTemplate[1] ? tktTemplate[1] : null}{" "}
          {drawTemplate[1] ? drawTemplate[1] : null}
        </div>
      </div>
    );
  }
}

export default KenoDraw;
