import React from "react";
import "./App.css";
import KenoDraw from "./components/kenoDraw";

function App() {
  return (
    <div className="App">
      <KenoDraw />
    </div>
  );
}

export default App;
